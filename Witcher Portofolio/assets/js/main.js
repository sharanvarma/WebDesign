$(document).ready(function () {
    
    // sticky navigation menu

    let nav_offset_top = $('.header_area').height() + 50;

    function navbarFixed() {
        if ($('.header_area').length) {
            $(window).scroll(function () {
                let scroll = $(window).scrollTop();
                if (scroll >= nav_offset_top) {
                    $('.header_area .main-menu').addClass('navbar_fixed');
                } else {
                    $('.header_area .main-menu').removeClass('navbar_fixed');
                }
            })
        }
    }

    navbarFixed();


    // Owl-carousel

    $('.site-main .client-area .owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            560: {
                items: 2
            }
        }
    })

    //Pop-Up Gallery for Projects JS Code

    let $btns = $('.projects .button-group button');


    $btns.click(function (e) {

        $('.projects .button-group button').removeClass('active');
        e.target.classList.add('active');

        let selector = $(e.target).attr('data-filter');
        $('.projects .grid').isotope({
            filter: selector
        });

        return false;
    })

    $('.projects .button-group #btn1').trigger('click');

    $('.projects .grid .test-popup-link').magnificPopup({
        type: 'image',
        gallery: { enabled: true }
    });

});

//Smooth Scroll Code Base Code

function smoothScroll(target, duration){
    var target = document.querySelector(target);
    var targetPosition = target.getBoundingClientRect().top;
    var startPosition = window.pageYOffset;
    var distance = targetPosition - startPosition;
    var startTime = null;

    function animation(currentTime){
        if(startTime === null){
            startTime = currentTime;
        }
        var timeElapsed = currentTime - startTime;
        var run = ease(timeElapsed, startPosition, distance, duration);
        window.scrollTo(0,run);
        if(timeElapsed < duration) requestAnimationFrame(animation);
    }
    
    function ease(t, b, c, d){
        t /= d/2;
        if(t<1) {
            return c/2 * t * t+b;
        }
        t--;
        return -c/2*(t*(t-2)-1)+b;
    }

    requestAnimationFrame(animation);
}

//Smooth Scroll Implementation For Current Program

var navHome = document.querySelector('.navHome');
var navAbout = document.querySelector('.navAbout');
var navServices = document.querySelector('.navServices');
var navShowcase = document.querySelector('.navShowcase');
var navClients = document.querySelector('.navClients');
var navProjects = document.querySelector('.navProjects');
var navSubscribe = document.querySelector('.navSubscribe');

navHome.addEventListener('click', function(){
    smoothScroll('.site-banner',1000);
});
navAbout.addEventListener('click', function(){
    smoothScroll('.about-area',1000);
});
navServices.addEventListener('click', function(){
    smoothScroll('.services-area',1000);
});
navShowcase.addEventListener('click', function(){
    smoothScroll('.monsterShowcase',1000);
});
navClients.addEventListener('click', function(){
    smoothScroll('.clients-area',1000);
});
navProjects.addEventListener('click', function(){
    smoothScroll('.projects',1000);
});
navSubscribe.addEventListener('click', function(){
    smoothScroll('.subscribe',1000);
});

//Scroll Appear Code (For Single Section)

function scrollAppear(){
    var introText = document.querySelector('.text-intro');
    var introPosition = introText.getBoundingClientRect().top;
    var screenPosition = window.innerHeight/1.3;

    if(introPosition < screenPosition){
        introText.classList.add('text-appear');
    }
}

window.addEventListener('scroll',scrollAppear);

//Typewriter Effect 

const texts = ["Witcher", "Monster Hunter", "Gwent Champion", "Legendary White Wolf"];
var count = 0;
var index = 0;
let currentText = "";
let letter = "";

(function type(){
    if(count === texts.length){
        count = 0;
    }
    currentText = texts[count];
    letter = currentText.slice(0, ++index);

    document.querySelector('.typewriter').textContent = letter;
    if(letter.length === currentText.length){
        count++;
        index = 0;
    }
    setTimeout(type, 300);
})();